require 'fastlane/action'
require_relative '../helper/app_beta_helper'

module Fastlane
  module Actions
    class AppBetaAction < Action
      def self.run(params)
        require 'rest-client'

        token = self.get_token(params)

        project_id = params[:project_id]
        app_id = params[:app_id]
        version = params[:version]
        build = params[:build]

        file = File.new(params[:ipa_path], 'rb')

        applications_id = self.send_file(file, token, project_id, app_id, version, build)

        UI.message("File uploaded! Application id -> #{applications_id}")

        return applications_id
      end

      def self.get_token(params)
        token = params[:api_token]
        
        unless token
          UI.message("API token not found")

          unless params[:user_login] or params[:user_password]
            raise "Bad auth pass APP_BETA_TOKEN(:api_token) or APP_BETA_USER_LOGIN(:user_login) and APP_BETA_USER_PASSWORD(:user_password)"
          end

          UI.message("Get auth token by username/password")

          params = {
            "username" => params[:user_login],
            "password" => params[:user_password]
          }

          begin
            response = RestClient.post("#{self.base_api_url}auth-token/", 
                                        params.to_json, 
                                        {
                                          content_type: :json, 
                                          accept: :json
                                        }
                                      )
          rescue RestClient::ExceptionWithResponse => e
            raise "ERROR GET TOKEN #{e.response}"            
          end

          UI.message("API token received")

          token = JSON.parse(response.body)["token"]
        end
        
        return token
      end

      def self.send_file(file, token, project_id, app_id, version, build)
        headres = {
          "Authorization" => "Token #{token}"
        }

        params = {
          "app_id" => app_id,
          "version" => version,
          "build" => build,
          "project" => project_id,
          "build_file" => file
        }

        begin
          response = RestClient.post("#{self.base_api_url}applications/", 
                                      params,
                                      headres
                                    )
        rescue RestClient::ExceptionWithResponse => e
          raise "ERROR SEND APP #{e.response}"            
        end

        return JSON.parse(response.body)["id"]
      end

      def self.base_api_url 
        "https://appbeta.macwins.kz/api/"
      end

      def self.description
        "Upload files on AppBeta Project"
      end

      def self.authors
        ["Eugene"]
      end

      def self.details        
        "Upload build files on test server AppBeta project"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :api_token,
                                   env_name: "APP_BETA_TOKEN",
                                description: "AppBeta token",
                                   optional: true,
                                       type: String),
          FastlaneCore::ConfigItem.new(key: :user_login,
                                  env_name: "APP_BETA_USER_LOGIN",
                               description: "AppBeta login",
                                  optional: true,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :user_password,
                                  env_name: "APP_BETA_USER_PASSWORD",
                               description: "AppBeta password",
                                  optional: true,
                                      type: String),

          FastlaneCore::ConfigItem.new(key: :project_id,
                                  env_name: "APP_BETA_PROJECT_ID",
                               description: "AppBeta project id for uploading *.ipa",
                                  optional: false,
                                      type: Integer),
          FastlaneCore::ConfigItem.new(key: :app_id,
                                  env_name: "APP_BETA_APP_ID",
                               description: "AppBeta app id for uploading *.ipa",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :version,
                                  env_name: "APP_BETA_APP_VERSION",
                               description: "AppBeta app version for uploading *.ipa",
                                  optional: false,
                                      type: String),
          FastlaneCore::ConfigItem.new(key: :build,
                                  env_name: "APP_BETA_BUILD_NUMBER",
                               description: "AppBeta build number for uploading *.ipa",
                                  optional: false,
                                      type: Integer),
          FastlaneCore::ConfigItem.new(key: :ipa_path,
                                  env_name: "APP_BETA_IPA_PATH",
                               description: "AppBeta *.ipa path",
                                  optional: true,
                                      type: String,
                              verify_block: proc do |value|
                                              raise "Couldn't find file at path '#{value}'".red unless File.exist?(value)
                                            end)
        ]
      end

      def self.is_supported?(platform)
        [:ios].include?(platform)
      end
    end
  end
end
