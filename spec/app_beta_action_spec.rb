describe Fastlane::Actions::AppBetaAction do
  describe '#run' do
    it 'prints a message' do
      expect(Fastlane::UI).to receive(:message).with("The app_beta plugin is working!")

      Fastlane::Actions::AppBetaAction.run(nil)
    end
  end
end
